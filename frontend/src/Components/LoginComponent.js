import React, { useState } from 'react';
import '../Styles/Login.css';
import { Link } from 'wouter';

const LoginComponent = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleLogin = (e) => {
    e.preventDefault();
    // Add your login logic here
    window.location.href = '/home';
  };

  return (
    <div className='login-container'>
      <form className="login accent vertical" onSubmit={handleLogin}>
        
      <h2>Login to Game Shack</h2>
      <div className="form-group">
        <label>Username</label>
        <input type="username" class="form-control" id="username" placeholder="Enter username" value={username}
              onChange={handleUsernameChange}
              required/>
      </div>
      <div className="form-group">
        <label>Password</label>
        <input type="password" class="form-control" id="password" placeholder="Enter password" value={password}
              onChange={handlePasswordChange}
              required/>
      </div>
      <button type="submit" class="btn btn-primary">Login</button>
      <div className="register-link">
          Don't have an account? <Link className='link' to="/register">Register here</Link>
        </div>
      </form>
    </div>
  );
};

export default LoginComponent;
