import React, { useState } from 'react';
import '../Styles/Register.css';
import { Link } from 'wouter';

const RegisterComponent = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleFirstNameChange = (e) => {
    setFirstName(e.target.value);
  };

  const handleLastNameChange = (e) => {
    setLastName(e.target.value);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleConfirmPasswordChange = (e) => {
    setConfirmPassword(e.target.value);
  };

  const handleRegister = (e) => {
    e.preventDefault();
    // Add your registration logic here
  };

  return (
    <div className="register-container">
    <form onSubmit={handleRegister} className='register accent vertical-form'>
        <h2>Register to Game Shack</h2>
        <div className="row">
            <div className="col">
                <label>First Name</label>
                <input type="text" className="form-control" id="firstName"
                    value={firstName}
                    onChange={handleFirstNameChange}
                    required />
            </div>
            <div className="col">
                <label>Last Name</label>
                <input type="text" className="form-control" id="lastName"
                    value={lastName}
                    onChange={handleLastNameChange}
                    required/>
            </div>
        </div>
        <div className="form-group">
          <label>Email</label>
          <input className='form-control'
            type="email"
            id="email"
            value={email}
            onChange={handleEmailChange}
            required
          />
        </div>

        <div className="form-group">
          <label>Username</label>
          <input className='form-control'
            type="username"
            id="username"
            value={username}
            onChange={handleUsernameChange}
            required
          />
        </div>

        <div className="row">
            <div className="col">
                <label>Password</label>
            <input className='form-control'
                type="password"
                id="password"
                value={password}
                onChange={handlePasswordChange}
                required
            />
            </div>
            <div className="col">
            <label>Confirm Password</label>
                <input className='form-control'
                    type="password"
                    id="confirmPassword"
                    value={confirmPassword}
                    onChange={handleConfirmPasswordChange}
                    required
                />
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Register</button>

        <div className="login-link">
          Already have an account? <Link className='link' to="/">Login here</Link>
        </div>
    </form>
    </div>
  );
};

export default RegisterComponent;
