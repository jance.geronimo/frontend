package com.Novare.SampleEcommerce.Controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import com.Novare.SampleEcommerce.Repository.Product;
import com.Novare.SampleEcommerce.Service.APIResponse;
import com.Novare.SampleEcommerce.Service.ProductService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @PostMapping("/")
    public Product addProduct(@RequestBody Product product){
        return productService.save(product);
    }

    // @PostMapping("/bulk")
    // public Product addProducts(@RequestBody List<Product> product){
    //     return productService.saveAll(product);
    // }

    @GetMapping("")
    public List<Product> getAllProduct(){
        return productService.findAll();
    }

    @GetMapping("/{field}")
    private APIResponse<List<Product>> getProductWithSort(@PathVariable String field){
        List<Product> allProducts = productService.findProductsWithSorting(field);
        return new APIResponse<>(allProducts.size(), allProducts);
    }

    @GetMapping("/page/{offset}")
    public APIResponse<List<Product>> getProductPagination(@RequestParam(defaultValue = "0") int offset) {
        Page<Product> productPage = productService.findProductByPagination(offset);
        return new APIResponse(productPage.getSize(), productPage);
    }
}
