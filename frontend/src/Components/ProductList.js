import { Component } from "react";
import ProductService from "../Services/ProductService";
import Header from "./Header";
import '../Styles/Product.css'

class ProductList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      offset: 0,
      pageSize: 10,
      totalPage: 0,
      totalProduct: 0,
    };
  }
  componentDidMount() {
    ProductService.getProducts().then((res) => {
        this.setState({products: res.data})
    })
  }
  render() {
    return (
      <>
        <Header/>
        <div className="product-container">
          <h2>Products</h2>
          <div style={{ display: "flex", flexWrap: "wrap", gap: "8px" }}>
            {this.state.products.map((product) => (
              <div
                key={product.id}
                style={{
                  padding: "8px",
                  border: "2px grey solid",
                  width: "160px",
                }}
              >
                <img
                  // src={product.image}
                  alt={product.image}
                  style={{ width: "100%" }}
                />
                <p style={{ fontSize: ".8rem" }}>
                  <strong>{product.name}</strong>
                </p>
                <p style={{ fontSize: ".6rem" }}>{product.description}</p>
                <p style={{ fontSize: ".6rem" }}>{product.price}</p>
                <p style={{ fontSize: ".6rem" }}>{product.genre}</p>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}

export default ProductList;