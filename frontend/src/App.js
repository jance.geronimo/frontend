import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import { Link, Route, Switch } from 'wouter';

import ProductList from "./Components/ProductList";
import Header from './Components/Header';
import LoginComponent from './Components/LoginComponent';
import RegisterComponent from './Components/RegisterComponent';
import CartComponent from './Components/CartComponent';

function App() {
  return (
    <div>
    {/* <header>
        <div style={{ display: "flex"}}>
          <Link style={{margin: "20px"}} href="/product">Home</Link>
          <div style={{ flexGrow: 1 }} />
        </div>
      </header>
       */}
       {/* <Header/> */}
      {/* < ProductList /> */}
      {/* <LoginComponent></LoginComponent> */}
      <Switch>
        <Route path="/" component={LoginComponent} />
        {/* <Route path="" component={Header}/> */}
        <Route path="/register" component={RegisterComponent}/>
        <Route path="/home" component={ProductList}/>
        <Route path="/cart" component={CartComponent}/>
      </Switch>
    </div>
  );
}

export default App;
