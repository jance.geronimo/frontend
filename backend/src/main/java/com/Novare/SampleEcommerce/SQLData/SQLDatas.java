package com.Novare.SampleEcommerce.SQLData;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.Novare.SampleEcommerce.Repository.Product;
import com.Novare.SampleEcommerce.Service.ProductService;

@Component
public class SQLDatas implements CommandLineRunner {

    @Autowired
    private ProductService productService;

    @Override
    public void run(String... args) throws Exception {
        populateProductTable();
    }

    private void populateProductTable() {
        for (int x = 0; x <= 100; x++) {
            Product product = new Product();
            product.setId(x);
            product.setName(generateRandomWords(1));
            product.setImage("[This is an image]");
            product.setDescription("Lorem ipsum");

            Double price = Math.random() * 20000;
            product.setPrice(price);

            product.setGenre("[This is a Genre]");

            productService.save(product);
        }
    }

    private String generateRandomWords(int numberOfWords) {
        String[] randomStrings = new String[numberOfWords];
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            char[] word = new char[random.nextInt(8) + 3]; // words of length 3 through 10. (1 and 2 letter words are
                                                           // boring.)
            for (int j = 0; j < word.length; j++) {
                word[j] = (char) ('a' + random.nextInt(26));
            }
            randomStrings[i] = new String(word);
        }
        return String.join("", randomStrings);
    }

}
