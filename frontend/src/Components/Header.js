import React from 'react';
import { Link, Route, Switch } from 'wouter';
import LoginComponent from './LoginComponent';
import { BsCart } from "react-icons/bs";

const Header = () => {
  const handleLogout = () => {
    // Add your login logic here
    
    window.location.href = '/';
  };

  return (
    <nav>
      <Link className="company" to='/home'>Game Shack</Link>
      <div className="right-section">
        <Link to='/cart'
            // onClick={toggleSidebar}
        >
            <BsCart className='cart-icon'/>
        </Link>
        <button className="btn btn-primary"
        onClick={handleLogout}
        >Logout</button>
      </div>
    {/* <Switch>
        <Route path="/login" component={LoginComponent} />
    </Switch> */}
    </nav>
  );
};

export default Header;