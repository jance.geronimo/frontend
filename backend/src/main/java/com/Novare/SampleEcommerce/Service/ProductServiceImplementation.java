package com.Novare.SampleEcommerce.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.Novare.SampleEcommerce.Repository.Product;
import com.Novare.SampleEcommerce.Repository.ProductRepository;

@Service
public class ProductServiceImplementation implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    };

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    };

    @Override
    public Product findById(long id) {
        // return productRepository.findById(id).orElseThrow(() -> new Exce);
        return null;
    };

    @Override
    public void deleteById(long id) {

    };

    @Override
    public Product updateById(long id, Product product) {
        return null;
    };

    @Override
    public Page<Product> findProductByPagination(int offset) {
        Page<Product> productPage = productRepository.findAll(PageRequest.of(offset, 10));
        return productPage;
    }

    @Override
    public List<Product> findProductsWithSorting(String field) {
        return productRepository.findAll(Sort.by(Sort.Direction.ASC, field));
    }

}
